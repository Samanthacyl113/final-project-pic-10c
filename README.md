# PIC 10C Final Project by Samantha Chung

## Description

To have a game in which an empty pizza face appears on the screen, the goal is to add your favorite ingredients to the pizza. To do so, the user will have to answer certain math questions correctly!

### A youtube video link to show how the project works can be found below:

https://www.youtube.com/watch?v=ntZv67Jtrkw

### Notes

Towards the end, some addiitonal files such as sub_window_one.cpp which was supposed to represent the level one questions from the branch "trying_new_levels" ended up appearing on master branch in my bit bucket source. I had to remove these files using git bash. Additionally, I removed files that I previously created but removed locally on bit bucket such as Dialog.h and Dialog.cpp in case they cause issues with cloning or running the project.

## Citations

used the following images as a guide to designing my own widgets:

“Chicken Cartoon.” CLEAN PNG, www.cleanpng.com/png-clip-art-ham-image-food-meat-6478394/.

### Resources consulted

“C++ Game Tutorial 1 : drawing the player (rectangle)”, YouTube, Uploaded by Abdullah Aghazada, November 13, 2014. https://www.youtube.com/watch?v=8ntEQpg7gck&list=PLyb40eoxkelOa5xCB9fvGrkoBf8JzEwtV

"C++ Game Tutorial 2 – Moving With the Arrow Keys”, YouTube, Uploaded by Abdullah Aghazada, November 14, 2014. https://www.youtube.com/watch?v=kvrAAP_ayWI&list=PLyb40eoxkelOa5xCB9fvGrkoBf8JzEwtV&index=2

“C++ Game Tutorial 3 – Shooting  With the Space Bar”, YouTube, Uploaded by Abdullah Aghazada, November 18, 2014.  https://www.youtube.com/watch?v=Fgwn_ENPL8c&list=PLyb40eoxkelOa5xCB9fvGrkoBf8JzEwtV&index=3

“C++ Game Tutorial 8 – Adding Graphics”, YouTube, Uploaded by Abdullah Aghazada, November 29, 2014. https://www.youtube.com/watch?v=xPs40BrYHkg&list=PLyb40eoxkelOa5xCB9fvGrkoBf8JzEwtV&index=8

“C++ Game Tutorial 23 – Implementing the Button Class (Part 1)”, YouTube, Uploaded by Abdullah Aghazada, February 28, 2015. https://www.youtube.com/watch?v=o_Ozdyz73Bw&list=PLyb40eoxkelOa5xCB9fvGrkoBf8JzEwtV&index=23

“C++ Game Tutorial 24 – Implementing the Button Class (Part 2)”, YouTube, Uploaded by by Abdullah Aghazada, February 28, 2015.  https://www.youtube.com/watch?v=6M9AKQ7NPBk&list=PLyb40eoxkelOa5xCB9fvGrkoBf8JzEwtV&index=24

“QInputDialog Class.” Qt Documentation, doc.qt.io/qt-5/qinputdialog.html#getInt.

“QPushButton Class.” Qt Documentation, doc.qt.io/qt-5/qpushbutton.html. Accessed May 10, 2020.

“Signals and Slots.” Qt Documentation, https://doc.qt.io/qt-5/signalsandslots.html. Accessed May 11, 2020.

“Keys QML Type.” Qt Documentation, doc.qt.io/qt-5/qml-qtquick-keys.html#digit8Pressed-signal.

“How to Catch Enter Key.” Wiki.qt.io, wiki.qt.io/How_to_catch_enter_key.

“How to Use QPush Buttom.” Qt Documentation. https://wiki.qt.io/How_to_Use_QPushButton. Accessed May 11, 2020.

“Widgets Tutorial – Child Widgets Tutorial – Child Widgets.” Qt Documentation, https://doc.qt.io/qt-5/qtwidgets-tutorials-widgets-childwidget-example.html. Accessed May 11, 2020.

admin. “Qt T Beginners – Adding Click Event to QPushbutton Example.” Codebind, Codebind, 15 Sept. 2016, www.codebind.com/cpp-tutorial/qt-tutorial/qt-qpushbutton-click-event/.

“StackExchangeOverflow.” StackExchangeOverflow, Arlen, 22 Dec. 2011, https://stackoverflow.com/questions/8598693/qt-framework-how-to-display-a-qgraphicsview-in-a-layout.

“QMessageBox Class.” Qt Documentation, doc.qt.io/qt-5/qmessagebox.html. Accessed May 29, 2020.

“QGraphicsScene Class.” Qt Documentation, doc.qt.io/qt-5/qgraphicsscene.html#foregroundBrush-prop.

Skearney, “C2143: Syntax Error: Missing ‘;’ before ‘*’ & C4430 missing type specifier -int assumed. Note: C++ does not support default int.” StackOverflow. StackOverFlow. 29 April, 2016. Published. StackOverFlow. Accessed June 7, 2020. 


## Learning Log

**May 6th, 2020**

- Watched a tutorial about the beginnings of a qt Game, including how to create widgets a simple rectangle, how to make it move, and how to visualize it on a scene, what a signal and a slot is 

- I realized that I may have to simplify my game a bit more.

- This is my plan so far: 

- To have boxes with words : ingredients, cheese, meat, etc.

- To have a signal that when clicked on a particular box, pops up a math problem

- The user will have to enter the correct answer on the keyboard before the ingredient gets placed on the pizza.

- I anticipate the following issues: “How to update” the widget -> delete a widget and change it to something else, how to insert graphics, how to space out the widgets, slots, and signals

- At this point, I consulted various resources such as online tutorials the C++ game tutorial series, and this is also documented in the resources list of this readme file.

- I followed the code close to exactly so that I can start the learning process.

**May 8th, 2020**

- Made a rectangle using QGraphicsRectItem and learnt how to make it respond to keys as well as looked at key documentations.I also learnt how to set the dimensions of the rectangle.

- At this point, I also followed the close closely from C++ game tutorials and documentations, mainly C++ game tutorial 1 and 2, this is so that I can start the learning process of using Qt creator.

**May 10, 2020**

- Drew a pizza face item from paint and added it to the scene, I struggled with this part because the video that I was watching used files such as QGraphicsItem and QImage that I did not quite understand.

- I also followed the "C++ Game Tutorial 9 - Adding Graphics" closely to learn about QGraphicsScene and QGraphicsView.

- It also took me a while to figure out how to use the resource files to add images. I managed to visualized the pizza face on the screen and to make it move in response to keys.


**May 11, 2020**

- I struggled with signals and slots. My goal was to be able to implement a push button for certain "ingredients", however when I tried using the Pushbutton signal, and passing it into the scene, C++ compiler kept giving me an error of "Abstract classes". I consulted various Qdocumentations such as relevant qt documentations and qt wikis to try to figure this problem out.

- to learn signals and slots I also made use of qt documentations and tutorials listed in the resources list.

**May 12, 2020**

- Figured out how to add a QPushButton Item in the main.cpp file. After looking at the documentation, QPushButton seems to inherit from QWidget, so I need to use the add Widget functionality of QGraphicsScene. I consulted the source “Widgets Tutorial – Child Widgets Tutorial – Child Widgets” as well as QGraphicsScene, QWidget, as well as QPushButton documentations. 

- I then looked into slots and signals, and tried to see what it looks like in Qt creator if I created a new scene the way I did it in main.cpp

- I was able to successfully add the new scene


**May 13, 2020**

- I figured out how to connect QPushButton objects to slots and signals. Specifically, I figured out how to create a button, and when the mouse clicks the button, a new scene pops up with a math problem. I was initially struggling with aspect. However, I looked at a couple of documentation such as the ‘Qt T Beginners – Adding Click event to QPushButton Example” website and I removed the void clicked() declaration under signals as I think that clicked() is a default signal of QPushButton which can cause errors when compiling.

- I also uploaded various images of the different “ingredients”.

- My plan so far involves having different push buttons: tomato sauce, cheese, ham, more ham, and thick crust, each associated with a question and if the user enters the question correctly, the widget will update. This is a very simplified version of the game, and can continue to be updated to include better graphics, better user functionalities, once I figure out a simple form of the code.

**May 15, 2020**

- Attempted to figure out how to perform such an operation: if the user enters the answer correctly, the current widget will be updated. For example, in the “cheese button” if the user answers the math problem correctly, the widget will update. 

- I struggled with making this implementation work, I had a few ideas, either to create this widget in the new class and then call the class if the key is pressed. This can be done by implementing a bool function to check if the right key is pressed by the user, and if the bool function returns true in the main.cpp, then the new widget object constructor is called. The QKeyEvent *Event argument, as detailed in the tutorial I watched, was giving me some issues as the compiler was giving a warning suggesting uninitialized variables. 

- In addition, I realized that my previous widget, one that I initially created was still set in focus, hence, I need to figure out a way to set the new object in focus if I want it to respond to a key.

- Perhaps, I need to use signals and slots. 

- I looked at the Qt documentation for “Keys QML type” to see if there is a signal associated with pressing number keys.


**May 16, 2020**

- I created a Boolean function who takes in QEvent *event as an argument and returns true or false depending on whether or not the right key is pressed. Then, I wanted to take that in main.cpp and if it returns true, update the widget. However, I am unable to pass a parameter of typeQEvent*event. I consulted the “How to Catch Enter Key” website for how to code for a Boolean function that takes a QEvent parameter.

- My program crashed when I tried to initialize QEvent *event to nullptr


**May 17, 2020**

- Researched on the QInput Dialog class. Initially, when I implemented the function as a method of the a QPushButton class, it was not connected to the button, (aka the dialog will appear regardless of whether or not the button was pushed). Consulted the Qt documentation file for QInput Dialog. I Was able to get to the point where the dialog QInput responds to a QPushbutton object


**May 19, 2020**

- By using the qdebug function I was able to figure out that my error was in the int main.cpp file, I had the getter function access the private member variable, but the private member variable called the “user_enter_variable”  is set in another function that acts as a slot to the clicked() signal of QPushButton. Then I had an if statement that calls the getter function, and if the user answers the correct answer, in this case, the desired value is 8, I will add another widget to the scene. The default value of the “user_enter_variable” is set to zero. However, I think that my if statement in int main.cpp is calling the default set value of user_enter_variable, as this is initialized when the object is called. I was able to find this mistake through using qdebug to keep track of the progress of the code. 

**May 20, 2020**

- Tried a different approach, where instead of applying the if (answer is correct) logic to the int main function, I will do this inside the button member function. 

- If the answer is correct, this pops up a new window with the updated widget. This works in the branch “window_test”. 

- However, because my goal is to add everything to one scene, this is what I will try to figure out once I have the general structure of my code figured out.


**May 23, 2020**

- I ran into the issue where when I tried to add buttons, the buttons overlapped.

- And when I tried use QVBoxLayout, I could not add the QGraphicsPixMap Item object(aka the designed widget, the "main" pizza face)

- I tried using the show() method, but I think this is because as discussed in class, Qt Widget creates a new child widget so it "replaces" the existing widget, which is why if I try to show two widgets, one disappears. 
 

**May 24, 2020**

- Fixed the issue of buttons overlapping in the main window scene, by looking at a sample code that a user called "Arlen" posted on the stackexchange forum titled : "Qt Framework: How to display a QGraphicsview in a layout?".

- Here is what I think is going on: We first create the Qv BoxLayout, QGraphics scene, QGraphicsView and QWidget(window). We create our items, as the object pizza_item refers to that main pizza face in the window. This is of type Qgraphicsitem pointer. We then add this to the scene, then we pass the scene into a view, and then add widgets to the layout. The QWidget *window->setLayout functionality takes in a layout pointer, so this is passed into a Qwidget item. Finally, we pass the QGraphics view pointer (of type QWidget) into another QWidget (window). We then call the .show functionality on window, to display everything. This is a system of parent - child widgets that allows us to add widgets together and display them nicely. As we learned in class, this is similar to the example with ints, the only difference is that we do not have to worry too much about memory management, unlike in regular C++ code. This is an example of "transfer of ownership" discussed in memory management.

**May 26, 2020**
 
- So far, I have 4 Qpushbuttons on my main screen : these are : cheese, ham, pineapple, and tomato.

- If I click on one, a QInputDialog pops up where the user can enter a value. 

- Hence, if the value is correct, a new widget in the screen pops up.

- I would like to figure out how to place widgets into one screen and if I sitll have time create a point system or learn how to code for different levels.


**May 29, 2020**

- I successfully used the QMessageBox to code for incorrect answer. 

- I want to try and implement another class that can account for the "game over" functionality. The goal is to for example, if the user answers correctly the quesiton in ham, instead of ham widget appearing on a pop up window, it gets added to this window and stays there upon the next ingredient.

- This window can also have a private member variable that tracks the buttons, for example, if I push the cheese ingredient button, it is one point, hence when all buttons have been pushed - this will yield a total score of four.

- once the score reaches 4, the user wins the game.

- One challenge that I anticipate is how to "extract" the value the user enters because these are local variables of local scope in the buttons classes.

- One way is to create a slot and signal system, where if the answer is correct, it sends a signal to the end_game_window class.

- This class can then take an integer that is unique to each ingredient, for example, cheese == 1, ham == 2. etc.

- This then adds one to the score - hence, if all the numbers have been called, a game over screen is displayed.


**May 30, 2020**

- My idea to implement a new class and track the score does not work. Upon testing using debug tools, I realized that my main.cpp does not seem to "update" the private member variable score_tracker which I created to track the score, it is incremented whenever a QPushButton object is pushed.

- This is probably due to the way Qt creator is structured. Everything shows upon the return on the window displayed to the user, and by that time, no signal has been emitted, this is why the score_tracker does not update.

- Perhaps I can try to create a local variable, a local score_tracker.

- Then I can claim that if the clicked() signal is emitted, update the score_tracker within the main.cpp file.

- finally, when the local score_tracker ==4, then I pop up the final window.


**May 31, 2020**

- I tried using a local variable called tracker, and after the code QObject::connect() we update the tracker counter.

- If tracker == 4 we call the final window.

- However, this method does not work because if we place this type of code in the int main window, the tracker will update regardless of whether or not the QPushButton is clicked.

- I plan on shifting gears a bit to focus on aesthetics such as designing the layout or designing a background for a bit.

**June 3, 2020**

- Set background color by looking at documentation for QGraphicsscene, however, it was not aesthetically pleasing

- After looking at discussion notes mainly from 5/12 and 5/14 discussion, I will try implementing my code in the mainwindow.cpp and mainwindow.h files instead

- I was able to successfully code for it and the game functions as it would in the int main file

- One thing interesting I noticed was that under the destructor, there was a call to delete ui, if I leave it, it will give a debug assertion fail error

- I think that this has to deal with memory leakage as what we learnt

- Because if I do not have anything under ui, it is trying to delete unassigned memory which leads to memory leakage

- In terms of trying to code for a "end game screen" (meaning that if I click all the buttons, a pop up screen with all the widgets will pop up) I branched out to a new branch called coding_main_window

- When I ran Qt creator, it was giving a bunch or errors such as "redefinition of class type" not allowed etc. Hence, I need to figure out, it may be an issue with the #include header files. 


** June 5, 2020**

- I tried to code for the "game_over" pop up window once all the QPushbuttons have been clicked. Still, I did not have any success. 

- I tried creating a local object in int main file of the class tracker_window(which is responsible for tracking if all the buttons have been pushed and then popping up a pop up window showing the user that they are successful).

- However, none of these gave me any success.

** June 7, 2020 **

- I tried to create a public object out of the tracker_window object and access its get-score function, so if the “score” == 4 (implying that if all the QPushButtons are clicked) than the final window is displayed. 

- I made it public to see if this will work, so I can call the object itself in the int main.cpp file. 

- Also, I was getting weird error messages from Qt Creator such as C2143 syntax error: Missing ‘;’ before ‘*” . 

- So I looked at stackexchange to try and identify the error, and then, I realized that the error was that I included “mainwindow.h” in a place where it does not need to be there. 

- I tried commenting it out and my code ran fine. I am curious as to why this is the case before I used #ifndef “mainwindow.h” functionalities in my code. 

**June 11, 2020**

- Figured out how to code for a final window to pop up once all the QPushButtons have been pressed.

- figured out by discussing with our class's TA on how to code for this functionality. I ended up defining a signal in tracker_window.cpp called all_buttons_pressed. Then I emitted the signal in increment_score if score == 4. Then I connected the signal with the slot final_window_display (which codes for the final window).

- My idea was to create a final_window_scene with all the custom widgets: cheese widget, ham widget, etc. However I kept getting a blank window(although a window did pop up, hence I have figured out how to code for the final_window through signal and slot system.

- I tried adding all the widgets to a QV Boxlayout object, then creating a QWidget object called window, then doing window->setLayout(layout), then calling show on window. But the result was not what I desired. 

**June 12, 2020**

- I successfully added all the QPixmap item widgets to the scene without them overlapping. After discussing with our class TA, I used the functionality setPos and setScale.

- Initially, I had the problem of the widgets overlapping and even though I used the setPos, this was not working and the widgets still kept overlapping.

- So after that, I watched the video "C++ Game Tutorial - Adding Graphics" and in the tutorial there were setting the position to something like (500,600). So I tried this method.

- Then, this did work and I added texts that indicate the completion of the game.
 
- Tried to experiment to see if I can code for different "levels" (e.g. if there are different difficulties of math questions).

FINAL REFLECTION:

- There were a couple of things that I initially thought about that I want to do or implement that I was not able to succesfully do. I had the idea of adding pushbuttons around the main widget in a circular form, but was not able to do so successfully. 

- Also, instead of having a final window pop up when all the QPushButton has been pushed, I initially wanted to have one widget add to the scene, one after the other.

- For example, if I click the cheese_button, and I answer it correctly, cheese gets added to the screen. Following that, if I click the "tomato_button" and answer the question correctly, then the tomato widget will be added.

- However, I was not able to successfully do this.

- Also, I want to figure out how to code for levels. For example, there is a main interface with levels and the math questions differ in their difficulty levels.



