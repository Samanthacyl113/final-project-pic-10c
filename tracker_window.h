
#ifndef TRACKER_WINDOW_H
#define TRACKER_WINDOW_H

#include "cheese_button.h"
#include "cheese_widget.h"
#include "ham_button.h"
#include "ham_widget.h"
#include "pineapple_button.h"
#include "pineapple_widget.h"
#include "tomato_button.h"
#include "tomato_widget.h"
#include "Widgetone.h"
#include <QApplication>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QWidget>
#include <QObject>
#include <QGraphicsPixmapItem>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QGraphicsView>

#include <QVBoxLayout>


class tracker_window: public QObject{

    Q_OBJECT
public:
    tracker_window();
    int get_score();

public slots:
    void increment_score();
    void final_window_display();

signals:
    void all_buttons_pressed();

private:
    int score;  //tracks the score
};

#endif
