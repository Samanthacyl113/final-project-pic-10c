#ifndef TOMATO_WIDGET_H
#define TOMATO_WIDGET_H
#include <QGraphicsPixmapItem>
#include <QImage>
#include <QPixmap>
#include <QKeyEvent>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QEvent>
#include <QObject>


class Tomato_Widget:public QGraphicsPixmapItem, public QImage, public QPixmap, public QWidget{
public:
    Tomato_Widget();
//default constructor to create the tomato widget

};

#endif // TOMATO_WIDGET_H
