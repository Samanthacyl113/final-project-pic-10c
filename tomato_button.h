#ifndef TOMATO_BUTTON_H
#define TOMATO_BUTTON_H

#include <QPushButton>
#include <QObject>
#include <QString>
#include <QKeyEvent>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QWidget>
#include <QInputDialog>
#include <QMessageBox>

class Tomato_Button:public QPushButton{
Q_OBJECT

public:
    Tomato_Button(); //default constructor for the tomato QPushbutton
public slots:
    void create_dialog(); //slot that creates the dialog input box
private:
};


#endif // TOMATO_BUTTON_H
