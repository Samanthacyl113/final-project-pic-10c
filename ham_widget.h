#ifndef HAM_H
#define HAM_H
#include <QGraphicsPixmapItem>
#include <QImage>
#include <QPixmap>
#include <QKeyEvent>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QEvent>
#include <QObject>

class ham_widget: public QGraphicsPixmapItem, public QImage, public QPixmap, public QWidget{
public:
    ham_widget();
};

#endif // HAM_H
