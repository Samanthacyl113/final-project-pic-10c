#include "cheese_button.h"
#include "cheese_widget.h"
#include <QKeyEvent>
#include <QInputDialog>
#include <QGraphicsScene>
#include <QGraphicsPixmapItem>
#include <QImage>
#include <QPixmap>
#include <QtDebug>

cheese_button::cheese_button(){
     this->setText("Cheese");

}

/* creates a dialog object, stores the inputted value in "answer"
 then creates a new window with the widget if the answer is correct
 */
void cheese_button::create_dialog(){
    QInputDialog dialog;
    int answer = 0;
    answer = QInputDialog::getInt(&dialog,"Enter your answer", "2*4 - Enter answer below", 0,-1000,1000,1); //obtains value eight
    while(answer!=8){
        QMessageBox message_box;

        message_box.setText("incorrect output - please try again");
        message_box.exec();
        answer = QInputDialog::getInt(&dialog,"Enter your answer", "2*4 ", 0,-1000,1000,1); //obtains value eight
            if(answer==8){
             break;
            }

        }

    cheese_widget *widget_cheese = new cheese_widget();
     QGraphicsScene * scene = new QGraphicsScene();
     scene->addItem(widget_cheese);
     scene->addText("Answer is correct!");
     QGraphicsView *view = new QGraphicsView();
     view->setScene(scene);
     view->show();

}


