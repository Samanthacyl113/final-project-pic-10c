#include "ham_button.h"
#include "ham_widget.h"
#include <QPushButton>
#include <QObject>
#include <QString>
#include <QKeyEvent>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QWidget>
#include <QInputDialog>
#include <QApplication>

ham_button::ham_button(){
    this->setText("Ham");
}
//sets a title to the button

//creates a dialog box for the user to input an answer
void ham_button::create_dialog(){
    QInputDialog dialog;
    int answer = 0;
    answer = QInputDialog::getInt(&dialog,"Enter your answer", "7*2 - Enter answer below", 0,-1000,1000,1); //obtains value eight
    while(answer!=14){
        QMessageBox message_box;
        message_box.setText("Incorrect answer - please try again");
        message_box.exec();
         answer = QInputDialog::getInt(&dialog,"Enter your answer", "7*2 ", 0,-1000,1000,1); //obtains value eight
        if(answer==14){
            break;
        }
    }
       ham_widget *widget_ham = new ham_widget();
        QGraphicsScene * scene = new QGraphicsScene();
        scene->addItem(widget_ham);
         scene->addText("Answer is correct!");
        QGraphicsView *view = new QGraphicsView();
        view->setScene(scene);
        view->show();
}
