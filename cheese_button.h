#ifndef BUTTON_ONE_H
#define BUTTON_ONE_H

#include <QPushButton>
#include <QObject>
#include <QString>
#include <QKeyEvent>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QWidget>
#include <QInputDialog>
#include <QMessageBox>
#include "cheese_widget.h"

//This class creates a button associated with the "cheese" ingredient

class cheese_button : public QPushButton{
    Q_OBJECT
public:
    cheese_button(); //default constructor for cheese button


 public slots:

  void create_dialog(); //slot that creates the dialog input box

signals:

 private:


};

#endif // BUTTON_ONE_H
