
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QObject>
#include <QWidget>
#include <QApplication>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QKeyEvent>
#include "Widgetone.h"
#include "cheese_button.h"
#include "cheese_widget.h"
#include <QEvent>
#include <QInputDialog>
#include <QDebug>
#include <QVBoxLayout>
#include "cheese_button.h"
#include"ham_button.h"
#include "ham_widget.h"
#include"pineapple_button.h"
#include "pineapple_widget.h"
#include "tomato_button.h"
#include "tomato_widget.h"
#include "Widgetone.h"
#include "tracker_window.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    tracker_window *window_tracker;
 signals:

 public slots:


private:
    Ui::MainWindow *ui;
    QVBoxLayout *layout; //organizes the layout of the scene

    //creating QPushButtons
    cheese_button *button_cheese;
    Tomato_Button *button_tomato;
    ham_button *button_ham;
    pineapple_button *button_pineapple;
    pizza *pizza_widget;


};
#endif // MAINWINDOW_H
