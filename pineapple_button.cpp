#include "pineapple_button.h"
#include "pineapple_widget.h"


//creates a pushbutton that responds to signals with the name "pineapple"
pineapple_button::pineapple_button(){
    this->setText("pineapple");
}

//creates a dialog box with the pineapple button with a question
void pineapple_button::create_dialog(){
    QInputDialog dialog;
    int answer = 0;
    answer = QInputDialog::getInt(&dialog,"Enter your answer", "4*4 - Enter answer below", 0,-1000,1000,1); //obtains value eight
    while(answer != 16){
        QMessageBox message_box;
        message_box.setText("Incorrect answer - please try again");
        message_box.exec();
        answer = QInputDialog::getInt(&dialog,"Enter your answer", "4*4 ", 0,-1000,1000,1); //obtains value eight
        if(answer == 16){
            break;
        }
    }

       pineapple_widget *widget_pineapple = new pineapple_widget();
        QGraphicsScene * scene = new QGraphicsScene();
        scene->addItem(widget_pineapple);
         scene->addText("Answer is correct!");
        QGraphicsView *view = new QGraphicsView();
        view->setScene(scene);
        view->show();

}
