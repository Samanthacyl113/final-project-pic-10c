#ifndef PINEAPPLE_BUTTON_H
#define PINEAPPLE_BUTTON_H
#include <QPushButton>
#include <QObject>
#include <QString>
#include <QKeyEvent>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QWidget>
#include <QInputDialog>
#include <QApplication>
#include <QMessageBox>


class pineapple_button: public QPushButton{
    Q_OBJECT
public:
    pineapple_button();
public slots:
    void create_dialog();
};

#endif // PINEAPPLE_BUTTON_H
