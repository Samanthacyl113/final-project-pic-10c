#ifndef HAM_BUTTON_H
#define HAM_BUTTON_H
#include <QPushButton>
#include <QObject>
#include <QString>
#include <QKeyEvent>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QWidget>
#include <QInputDialog>
#include <QApplication>
#include <QMessageBox>


class ham_button: public QPushButton{
    Q_OBJECT
public:
    ham_button();
public slots:
    void create_dialog();
};




#endif // HAM_BUTTON_H
