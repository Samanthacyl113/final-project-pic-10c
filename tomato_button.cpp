#include "tomato_button.h"
#include "tomato_widget.h"
#include <QPushButton>
#include <QObject>
#include <QString>
#include <QKeyEvent>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QWidget>
#include <QInputDialog>
#include <QApplication>

Tomato_Button::Tomato_Button(){
    this->setText("Tomato");
} //default constructor for QPushButton for the tomato ingredient

/* creates a dialog object, then stores the user entered value in the variable answer
 This will then create another window with the widget such as a pop up screen
 */
void Tomato_Button:: create_dialog(){
    QInputDialog dialog;
    int answer = 0;
    answer = QInputDialog::getInt(&dialog,"Enter your answer", "9*3 - Enter answer below", 0,-1000,1000,1); //obtains value eight
    while(answer !=27){
       QMessageBox message_box;
        message_box.setText("Incorrect answer - please try again");
        message_box.exec();
         answer = QInputDialog::getInt(&dialog,"Enter your answer", "9*3 ", 0,-1000,1000,1); //obtains value eight
       if(answer == 27){
         break;
        }
    }
       Tomato_Widget *widget_tomato = new Tomato_Widget();
        QGraphicsScene * scene = new QGraphicsScene();
        scene->addItem(widget_tomato);
         scene->addText("Answer is correct!");
        QGraphicsView *view = new QGraphicsView();
        view->setScene(scene);
        view->show();
}
