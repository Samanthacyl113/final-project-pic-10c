#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPushButton>
#include "tracker_window.h"

#include <iostream>

//Final working version for the project

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)

{


    // These parts of the code were extracted from discussion notes (discussion 5 -12 and 5 - 14

    QWidget *central_widget = new QWidget();
    layout = new QVBoxLayout();
    button_cheese = new cheese_button();
    button_ham = new ham_button();
    button_pineapple = new pineapple_button();
    button_tomato = new Tomato_Button();
    pizza_widget = new pizza(); //creates the pizza face on the main screen
    window_tracker= new tracker_window();

     button_cheese->setText("CHEESE");
     button_ham->setText("HAM");
     button_pineapple->setText("PINEAPPLE");
     button_tomato->setText("TOMATO");

    layout->addWidget(button_cheese);
    layout->addWidget(button_ham);
    layout->addWidget(button_pineapple);
    layout->addWidget(button_tomato);

    //the following lines of code as in the previous branch comes from a stackexchange user (logicwise) of how to add
    //QGraphicsPixMapItems to the list in a layout while creating a QGraphics View and QGraphics Scene

    /* This was answered by a  user called Arlen on December 22, 2011
      notably on the topic of  how to connect view and layout together
      URL linkage:
      https://stackoverflow.com/questions/8598693/qt-framework-how-to-display-a-qgraphicsview-in-a-layout
     */


    QGraphicsScene *scene = new QGraphicsScene();
    QGraphicsView *view = new QGraphicsView();
    scene->addItem(pizza_widget);
    view->setScene(scene);

    layout->addWidget(view);

    central_widget->setLayout(layout);
    setCentralWidget(central_widget);

    setCentralWidget(central_widget);



    //the following lines of code are used to connect signals and slots

    QObject::connect(button_cheese,SIGNAL(clicked()),button_cheese,SLOT(create_dialog()));
    QObject::connect(button_ham, SIGNAL(clicked()),button_ham,SLOT(create_dialog()));
    QObject::connect(button_pineapple,SIGNAL(clicked()), button_pineapple, SLOT(create_dialog()));
    QObject::connect(button_tomato, SIGNAL(clicked()), button_tomato, SLOT(create_dialog()));

    //connecting the QPushButtons with tracker_window, which is the pop up window for which I want to add all of my widgets
    //the current behaviour of the code is that when I answer a QInputDialog question correctly, a widget pops up
    //I want to have everything added onto one screen when all four questions are answered correctly


    QObject::connect(button_cheese, SIGNAL(clicked()), window_tracker, SLOT(increment_score()));
    QObject::connect(button_ham, SIGNAL(clicked()), window_tracker, SLOT(increment_score()));
    QObject:: connect(button_pineapple, SIGNAL(clicked()), window_tracker, SLOT(increment_score()));
    QObject::connect(button_tomato, SIGNAL(clicked()), window_tracker,SLOT(increment_score()));

    QObject::connect(window_tracker, SIGNAL(all_buttons_pressed()), window_tracker, SLOT(final_window_display()));
    //connecting all_buttons_pressed with the slot to display a new window display
}

MainWindow::~MainWindow()
{
    //delete ui;
}

