QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    Widgetone.cpp \
    cheese_button.cpp \
    cheese_widget.cpp \
    ham_button.cpp \
    ham_widget.cpp \
    main.cpp \
    mainwindow.cpp \
    pineapple_button.cpp \
    pineapple_widget.cpp \
    tomato_button.cpp \
    tomato_widget.cpp \
    tracker_window.cpp

HEADERS += \
    Widgetone.h \
    cheese_button.h \
    cheese_widget.h \
    ham_button.h \
    ham_widget.h \
    mainwindow.h \
    pineapple_button.h \
    pineapple_widget.h \
    tomato_button.h \
    tomato_widget.h \
    tracker_window.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    Images_widgets.qrc
