#include "mainwindow.h"
#include <QApplication>
#include <QWidget>
#include <QPushButton>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QVBoxLayout>
#include <QKeyEvent>
#include "Widgetone.h"
#include "cheese_button.h"
#include "cheese_widget.h"
#include "tomato_button.h"
#include "tomato_widget.h"
#include "ham_button.h"
#include "ham_widget.h"
#include "pineapple_button.h"
#include "pineapple_widget.h"
#include <QEvent>
#include <QInputDialog>
#include <QDebug>
#include "tracker_window.h"
#include <iostream>


/*   this version aims to create the "end game" window that I was previously working on but putting the code in mainwindow.cpp and seeing if that will work
 */

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);


     MainWindow w;


     w.show();

    return a.exec();


}
