#ifndef PINEAPPLE_WIDGET_H
#define PINEAPPLE_WIDGET_H
#include <QGraphicsPixmapItem>
#include <QImage>
#include <QPixmap>
#include <QKeyEvent>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QEvent>
#include <QObject>

class pineapple_widget: public QGraphicsPixmapItem, public QImage, public QPixmap, public QWidget{
public:
    pineapple_widget();
};

#endif // PINEAPPLE_WIDGET_H
