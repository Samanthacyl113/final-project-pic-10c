#include "tracker_window.h"
#include <iostream>
//to debug at this current state

tracker_window::tracker_window(){
    score = 0;
}

void tracker_window::increment_score(){
    score++;

    if(score==4){
        emit all_buttons_pressed();
    }
}

void tracker_window::final_window_display(){

    // QVBoxLayout *layout = new QVBoxLayout();
     QGraphicsScene *scene = new QGraphicsScene();
     QGraphicsView *view = new QGraphicsView();

    cheese_widget *widget_cheese = new cheese_widget();
     ham_widget *widget_ham = new ham_widget();
   Tomato_Widget *widget_tomato = new Tomato_Widget();
    pineapple_widget *widget_pineapple = new pineapple_widget();



    scene->addItem(widget_cheese);
    scene->addItem(widget_ham);
    scene->addItem(widget_tomato);
    scene->addItem(widget_pineapple);


    widget_cheese->setScale(0.7);
    widget_ham->setScale(0.7);
    widget_tomato->setScale(0.7);
    widget_pineapple->setScale(0.7);


    //all widgets added to the window and all are visible
    //I realized that my issue was that the coordinates I was initially testing out with were too small
    //I watched the youtube online resource: C++ Qt Game Tutorial 8 - Adding Graphics with URL link: https://www.youtube.com/watch?v=xPs40BrYHkg&list=PLyb40eoxkelOa5xCB9fvGrkoBf8JzEwtV&index=8
    //the youtube video I watched was using a setting of x and y coordinates in the 100's which was why I tried such big X and Y coordinates
    widget_cheese->setPos(-400,-400);
    widget_ham->setPos(-400,400);
    widget_tomato->setPos(400,-400);
    widget_pineapple->setPos(400,400);

    scene->addText("Congratuations you have finished the game!!!");


    view->setScene(scene);
    view->show();



    //my goal for this member function is to have all the widgets displayed at once in one window

}

int tracker_window:: get_score(){
    return score;
}


//adding this line of code and seeing if it works

